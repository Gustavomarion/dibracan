import { Component } from '@angular/core';
import { CmsService } from './service/cms.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'dibracan';


    constructor(private cmsService: CmsService) {
        const key = '$10$Y7onIakZGJ6uD2C4t47yauhjdNQkPEyYQFL4d/oUDXd8BTDwKOnD6';
        const domain = 'http://dibracam.com.br/';
        this.cmsService.setCredentials(
            key,
            domain
        );
    }
}
