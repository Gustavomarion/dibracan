import { HelperService } from './../../service/helper.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  //public toggled: boolean;

  constructor(public helper: HelperService) { }

  ngOnInit() {
  }

  //public toggle() {
    //this.toggled = this.toggled ? false : true;
//}

//public close(event) {
  //  if (event.target.id.includes('navbar')) {
  //      this.toggled = false;
  //  }
//}
}
