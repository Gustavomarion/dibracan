import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CmsService } from 'src/app/service/cms.service';
import { locales } from './states-cities';

@Component({
    selector: 'app-form-contact',
    templateUrl: './form-contact.component.html',
    styleUrls: ['./form-contact.component.scss']
})
export class FormContactComponent implements OnInit {
    @Input() public titleForm = 'Formulario';
    @Input() public buttonText = 'Solicitar Orçamento';

    @Input() public download;

    @Input() public subject = 'Contato';
    @Input() public emailConfig = {
        to_email: 'ne@dibracam.com.br',
        to_name: 'Dibracam',
        cc_emails: ['nasser@dibracam.com.br', 'ne@dibracam.com.br']
    };
    @Input() public semi: boolean;
    @Input() public used: boolean;

    public error: boolean;
    public loading: boolean;
    public submited: boolean;
    public form: FormGroup = this.fb.group({
        subject: [''],
        name: ['', Validators.required],
        email: ['', Validators.required],
        cel: ['', Validators.required],
        city: [''],
        uf: [''],
        aditional: [''],
        // vehicle: this.fb.group({
        //     brand: [''],
        //     model: [''],
        //     chassi: ['']
        // }),
        text: [''],
        to_email: [''],
        to_name: [''],
        cc_emails: [[]]
    });

    public locales: any[] = locales;

    constructor(
        private cmsService: CmsService,
        private fb: FormBuilder
    ) { }

    ngOnInit() {
        this.form.controls.subject.setValue(this.subject);

        if (this.emailConfig) {
            this.form.patchValue(this.emailConfig);
        } else {
            this.form.removeControl('to_email');
            this.form.removeControl('to_name');
            this.form.removeControl('cc_emails');
        }
    }

    public send() {
        this.error = false;
        this.loading = true;
        this.submited = true;
        this.form.controls.text.setValue(this.template(this.form.value));

        const data = this.form.value;
        this.cmsService.send(data).subscribe(response => {
            if (this.download) {
                this.cmsService.download(this.download.file, this.download.name);
            }
        }, error => {
            this.error = true;
        }, () => {
            this.loading = false;
            this.submited = true;
        });
    }

    public template(data) {
        return `
            <p><strong>Cidade:</strong> ${data.city}</p>
            <p><strong>Uf:</strong> ${data.uf}</p>
            <p><strong>Mensagem:</strong> ${data.aditional}</p>
        `;
        // <p><strong>Marca do Veiculo:</strong> ${data.vehicle.brand}</p>
        // <p><strong>Modelo do Veiculo:</strong> ${data.vehicle.model}</p>
        // <p><strong>Chassi do Veiculo:</strong> ${data.vehicle.chassi}</p>
    }

}
