import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdibracanComponent } from './adibracan.component';

describe('AdibracanComponent', () => {
  let component: AdibracanComponent;
  let fixture: ComponentFixture<AdibracanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdibracanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdibracanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
