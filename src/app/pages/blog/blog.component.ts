import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/service/cms.service';
import { ActivatedRoute } from '@angular/router';
import { PagerService } from 'src/app/service/pager.service';

@Component({
    selector: 'app-blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.component.scss'],
    providers: [PagerService]
})
export class BlogComponent implements OnInit {
    public posts = [];

    // pager object
    public pager: any = {};

    // paged items
    public pagedItems: any[];
    public activeCategory = '';
    public perPage: any;
    public load: boolean;

    constructor(
        public pagerService: PagerService,
        private cmsService: CmsService,
        private routerActive: ActivatedRoute
    ) { }

    ngOnInit() {
        this.perPage = 5;
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
        this.requisition();
    }

    setPage(page: number, total: number) {
        // get pager object from service
        this.pager = this.pagerService.getPager(total, page, this.perPage);
        // get current page of items
        this.pagedItems = this.posts.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    public changePage(page): void {
        this.requisition([], page);

    }

    private requisition(filterOptions = [], pageSelected = 1): void {
        // console.log(this.perPage)
        this.load = true;
        this.posts = [];
        this.cmsService.pullItems('blog/post').getAll({
            filter: filterOptions,
            quantidade: this.perPage,
            page: pageSelected
        }).subscribe((response: any) => {
            this.posts = response.data;
            this.setPage(response.current_page, response.total);
            this.load = false;
        });
    }

}
