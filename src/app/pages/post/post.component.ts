import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CmsService } from 'src/app/service/cms.service';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
    public post: any;
    @ViewChild('descript') public divDesc: ElementRef;
    constructor(private cmsService: CmsService, private activetedRoute: ActivatedRoute, private title: Title) { }

    ngOnInit() {
        this.activetedRoute.params.subscribe((params: any) => {

            this.cmsService.pullItems('blog/post').find(params.id, {tags: true, count_view: 1}).subscribe((post: any) => {
                console.log(post);
                this.post = post;
                this.title.setTitle(post.title + ' - BLOG Dibracam');
                this.divDesc.nativeElement.innerHTML = post.description;
                document.querySelectorAll('oembed[url]').forEach(element => {
                    console.log(element)
                    const anchor = document.createElement('a');
                    anchor.setAttribute('href', element.getAttribute('url'));
                    anchor.className = 'embedly-card';
                    element.appendChild(anchor);
                });
            });
        });
    }

}
