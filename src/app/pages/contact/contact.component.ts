import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CmsService } from 'src/app/service/cms.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
    public form: FormGroup = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        name: ['', Validators.required],
        cel: ['', Validators.required],
        subject: ['', Validators.required],
        text: ['', Validators.required],
        to_email: ['nasser@dibracam.com.br'],
        to_name: ['Vitor'],
        cc_emails: ['ne@dibracam.com.br']
    });

    public sended: boolean;

    
    constructor(private fb: FormBuilder, private cmsService: CmsService, private route: ActivatedRoute, private router: Router ) { }

    ngOnInit() {
        this.route.params.subscribe((params: any) => {
            this.sended = params.sended;
        });
    }

    send() {
        this.sended = true;
        const data = this.form.value;
        if(data.subject == 'trabalhe')
        {
            data.cc_emails.push('silvio_drh@dibracam.com.br');
        }
        this.cmsService.send(data).subscribe(() => {
            this.router.navigate(['contact', {sended: 1}]);
        });
    }
}
