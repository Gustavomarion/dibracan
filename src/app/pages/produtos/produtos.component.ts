import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/service/cms.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';


@Component({
    selector: 'app-produtos',
    templateUrl: './produtos.component.html',
    styleUrls: ['./produtos.component.scss']
})
export class ProdutosComponent implements OnInit {
    public categories: any = [];
    public slideConfig = {
        arrows: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
              breakpoint: 991,
              settings: {
                  centerMode: false,
                  centerPadding: '0px',
                  slidesToShow: 4
              }
          },
          {
              breakpoint: 836,
              settings: {
                  centerMode: false,
                  centerPadding: '0px',
                  slidesToShow: 2
              }
          },
          {
              breakpoint: 660,
              settings: {
                  centerMode: false,
                  centerPadding: '0px',
                  slidesToShow: 1
              }
          }

      ]
    };

    constructor(
        private cmsService: CmsService,
        private router: Router,
        public activatedRoute: ActivatedRoute,
        private breakpointObserver: BreakpointObserver
    ) { }

    ngOnInit() {
        // console.log(this.cmsService);
        this.activatedRoute.params.subscribe((params: any) => {
            // params.category;
            const options: any = {};
            if (params.category) {
                options.filter = [
                    ['categories.name', params.category]
                ];
            }
            this.getProducts(options);
        });
    }

    public getProducts(params: any = {}) {
        params.noPaginate = true;
        this.categories = [];
        this.cmsService.pullItems('produtos').getAll(params).subscribe((response: any) => {
            response.forEach((product: any) => {
                const newCategory: any = {
                    name:  product.category.replace(/[0-9]/g, ''),
                    original_name: product.category,
                    products: []
                };
                if (newCategory.name.includes('Locação') || newCategory.name.includes('Semi Novos') ) {
                    newCategory.blue_label = true;
                }
                const idx = this.categories.findIndex((category) => {
                    if (category.original_name.includes(product.category)) {
                        return true;
                    }
                });
                if (idx < 0) {
                    newCategory.products.push(product);
                    this.categories = this.categories.concat(newCategory);
                } else {
                    this.categories[idx].products.push(product);
                }

                // console.log(idx)
            });


            // console.log(this.categories);
        });
    }


    slickInit(e) {
        // console.log('slick initialized');
    }

    breakpoint(e) {
        console.log('breakpoint');
    }

    afterChange(e) {
        console.log('afterChange');
    }

    beforeChange(e) {
        console.log('beforeChange');
    }

}
