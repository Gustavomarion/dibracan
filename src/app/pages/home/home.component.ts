import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/service/cms.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public posts: any;
    public slideConfig = {
        arrows: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
          {
              breakpoint: 991,
              settings: {
                  centerMode: false,
                  centerPadding: '0px',
                  slidesToShow: 2
              }
          },
          {
              breakpoint: 836,
              settings: {
                  centerMode: false,
                  centerPadding: '0px',
                  slidesToShow: 1
              }
          },
          {
              breakpoint: 660,
              settings: {
                  centerMode: false,
                  centerPadding: '0px',
                  slidesToShow: 1
              }
          }

      ]
    };

    public slideConfig2 = {
        arrows: true,
        infinite: true,
        dots: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };

    public carousel: any = {};
    public categories: any;

    constructor(private cmsService: CmsService, private router: Router) { }

    ngOnInit() {
        this.cmsService.pullItems('blog/post').getAll({
            quantidade: 5
        }).subscribe((response: any) => {
            this.posts = response.data;
        });

        this.cmsService.pullItems('home').getAll({
            filter: [
                ['role', 'carousel'],
            ],
            noPaginate: true
        }).subscribe((response: any[]) => {
            this.carousel = response.shift();
        });


        this.cmsService.pullItems('categories').getAll({
            whereIn: [
            'name', ['01 Leves', '08 Ônibus', '09 Semi Novos', '10 Locação']
        ]}).subscribe((response: any) => {
            this.categories = response;
            console.log(response);
        });
    }


    slickInit(e) {
        console.log('slick initialized');
    }

    breakpoint(e) {
        console.log('breakpoint');
    }

    afterChange(e) {
        console.log('afterChange');
    }

    beforeChange(e) {
        console.log('beforeChange');
    }


    go({link}) {
        if(String(link).includes(location.host)) {
            let split = String(link).split('/');
            this.router.navigateByUrl(`/${split[split.length - 1]}`);
        } else {
            window.open(link, '_blank');
        }
    }
}
