import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/service/cms.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
    public product: any = {images: [{url: ''}]};
    public mainPhoto: string;
    public slideConfig = {
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1084,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                }
            }
        ]
    };

    public emailConfig: any = {
        to_email: 'nasser@dibracam.com.br',
        to_name: 'Dibracam',
        cc_emails: ['ne@dibracam.com.br']
    };

    constructor(
        private cmsService: CmsService,
        private route: ActivatedRoute,
        private fb: FormBuilder
    ) { }

    ngOnInit() {
        this.route.params.subscribe((params: any) => {
            this.cmsService.pullItems('produtos').find(params.id).subscribe((response) => {
                this.product = response;

                console.log(this.product)
                if( String(this.product.category).toLowerCase().includes('locação')) {
                    this.emailConfig.cc_emails.push('andre@dibracam.com.br');
                }

                if( String(this.product.category).toLowerCase().includes('semi')) {
                    this.emailConfig.cc_emails.push('usados@dibracam.com.br');
                }
                this.product.colors = this.product.colors ? JSON.parse(this.product.colors) : [];
                this.mainPhoto = this.product.images[0].url;
            });
        });
    }

    changeImage(photo) {
        console.log(photo)
    }

    slickInit(e) {
        console.log('slick initialized');
    }

    breakpoint(e) {
        // console.log('breakpoint');
    }

    afterChange(e) {
        // console.log(e.currentSlide);
        if (e.currentSlide === 0) {
            this.mainPhoto = this.product.images[0].url;
        } else {
            this.mainPhoto = this.product.photos[e.currentSlide - 1].path;
        }
    }

    beforeChange(e) {
        // console.log('beforeChange');
    }


    download() {
        this.cmsService.download(this.product.datasheet, this.product.title + '-Especificações Técnicas');
    }

}
