import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AdibracanComponent } from './pages/adibracan/adibracan.component';
import { PneusComponent } from './pages/pneus/pneus.component';
import { ServicosComponent } from './pages/servicos/servicos.component';
import { VenderComponent } from './pages/vender/vender.component';
import { BlogComponent } from './pages/blog/blog.component';
import { PostComponent } from './pages/post/post.component';
import { ContactComponent } from './pages/contact/contact.component';
import { ProdutosComponent } from './pages/produtos/produtos.component';
import { ItemComponent } from './pages/item/item.component';

const routes: Routes = [
  {
    path: '', pathMatch: 'full', redirectTo: 'home'
  },
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'adibracan', component: AdibracanComponent
  },
  {
    path: 'contact', component: ContactComponent
  },
  {
    path: 'pneus', component: PneusComponent
  },
  {
    path: 'servicos', component: ServicosComponent
  },
  {
    path: 'vender', component: VenderComponent
  },
  {
    path: 'blog', component: BlogComponent
  },
  {
    path: 'post/:id', component: PostComponent
  },
  {
    path: 'veiculos', component: ProdutosComponent
  },
  {
    path: 'veiculos/:id', component: ItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, scrollPositionRestoration: 'top'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
