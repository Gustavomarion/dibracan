import { HelperService } from './service/helper.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './layout/nav/nav.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NewsletterComponent } from './layout/newsletter/newsletter.component';
import { PneusComponent } from './pages/pneus/pneus.component';
import { ServicosComponent } from './pages/servicos/servicos.component';
import { VenderComponent } from './pages/vender/vender.component';
import { AdibracanComponent } from './pages/adibracan/adibracan.component';
import { BlogComponent } from './pages/blog/blog.component';
import { LayoutModule } from '@angular/cdk/layout';
import { PostComponent } from './pages/post/post.component';
import { ProdutosComponent } from './pages/produtos/produtos.component';
import { CmsService } from './service/cms.service';
import { HttpClientModule } from '@angular/common/http';
import { ItemComponent } from './pages/item/item.component';
import { FormContactComponent } from './layout/form-contact/form-contact.component';


@NgModule({
    declarations: [
        AppComponent,
        NavComponent,
        FooterComponent,
        HomeComponent,
        ContactComponent,
        NewsletterComponent,
        PneusComponent,
        ServicosComponent,
        VenderComponent,
        AdibracanComponent,
        BlogComponent,
        PostComponent,
        ProdutosComponent,
        ItemComponent,
        FormContactComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        LayoutModule,
        HttpClientModule,
        SlickCarouselModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [HelperService, CmsService],
    bootstrap: [AppComponent]
})
export class AppModule { }
